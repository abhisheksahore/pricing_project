const price_toggler_switch_checkbox = document.getElementById('toggle-price')
const toggle_price__price_basic_element = document.getElementById('price-basic');
const toggle_price__price_professional_element = document.getElementById('price-professional');
const toggle_price__price_master_element = document.getElementById('price-master');
const basic_button_element = document.querySelector('#basic-btn')
const professional_button_element = document.querySelector('#professional-btn')
const master_button_element = document.querySelector('#master-btn')

const monthly_basic_price = 19.99;
const monthly_professional_price = 24.99;
const monthly_master_price = 39.99;
const annual_basic_price = 199.99;
const annual_professional_price = 249.99;
const annual_master_price = 399.99;


price_toggler_switch_checkbox.addEventListener('change', function(event) {
    event.preventDefault();
    if(this.checked) {
        toggle_price__price_basic_element.innerText = monthly_basic_price;
        toggle_price__price_professional_element.innerText = monthly_professional_price;
        toggle_price__price_master_element.innerText = monthly_master_price;
    } else {
        toggle_price__price_basic_element.innerText = annual_basic_price;
        toggle_price__price_professional_element.innerText = annual_professional_price;
        toggle_price__price_master_element.innerText = annual_master_price;
    }
})

basic_button_element.addEventListener('mousedown', function() {
    basic_button_element.classList.add('accent-button-clicked');
})
basic_button_element.addEventListener('mouseup', function() {
    basic_button_element.classList.remove('accent-button-clicked');
})

professional_button_element.addEventListener('mousedown', function() {
    professional_button_element.classList.add('no-border-button-clicked');
})
professional_button_element.addEventListener('mouseup', function() {
    professional_button_element.classList.remove('no-border-button-clicked');
})

master_button_element.addEventListener('mousedown', function() {
    master_button_element.classList.add('accent-button-clicked');
})
master_button_element.addEventListener('mouseup', function() {
    master_button_element.classList.remove('accent-button-clicked');
})